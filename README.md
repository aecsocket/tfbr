# TFBR: Task Force Blockhead Radio

Provides custom chat strategies for handling chat types, such as area and radio chat, by configuration.
Supports custom radio items using [Sokol](https://gitlab.com/aecsocket/sokol).

---

This plugin is based off of ARMA 3's Task Force Arrowhead Radio (TFAR) mod, which provides advanced
area chat and radio chat, featuring frequencies and channels, and radio interference. TFBR attempts
to simulate these effects, and every feature and value is customisable.

The different chat strategies are triggered by a prefix in the user's chat, which supports permissions.
This is less cumbersome than typical plugins, which use commands or similar to switch channels
(`/c global`, `/c local`, etc.). Instead, a user can simply type their message with a prefix, e.g.
`!I am now shouting!` to chat in a specific way.

## Paper

TFBR is exposed as a Paper plugin, with its own configuration file.

### Dependencies

* [Java >=16](https://adoptopenjdk.net/?variant=openjdk16&jvmVariant=hotspot)
* [Paper >=1.17.1](https://papermc.io/)
* [Minecommons >=1.2](https://gitlab.com/aecsocket/minecommons)
* [Sokol >=1.2](https://gitlab.com/aecsocket/sokol)
* [ProtocolLib >=4.7.0](https://www.spigotmc.org/resources/protocollib.1997/)

### [Download](https://gitlab.com/api/v4/projects/27141550/jobs/artifacts/master/raw/paper/build/libs/tfbr-paper-1.1.jar?job=build)

### Documentation

The plugin is configured through `settings.conf`. The description of each config can be found there.

### Permissions

Each command has its own permission, prefixed with `tfbr.command` e.g. `/tfbr fq get`'s permission
would be `tfbr.command.fq.get`. For some commands, permission can be granted to allow a user to
run the command on someone else. In this case, the permission is suffixed with `others` e.g.
for `/tfbr fq get [another user]`, the permission is `tfbr.command.fq.get.others`.

### Commands

#### Format

`<required> [optional]`

For frequency fields: this can be in the format of `[number]{Hz,KHz,MHz,GHz}` e.g. `116.4MHz`.
If a unit is not specified, it is interpreted as an internal frequency value (MHz).

#### Commands

Root command: `/tfbr`

* `help [query]` Lists help information (everyone has permission)
* `version` Gets version information (everyone has permission)
* `reload` Reloads all plugin data
* `fq get [target]` Gets a player's frequency (supports `others`)
* `fq set <frequency> [target]` Sets a player's frequency (supports `others`)
* `fq unset [target]` Unsets a player's frequency (supports `others`)
* `fq info <frequency> [compare]` Gets the information on a specific frequency
* `fq bc <frequency> <power> [content]` Broadcasts a message on a frequency
* `fq noise <power> <to> [from]` Calculate the radio noise from one location to another
* `noise <noise> [message]` Add noise to a message

## Development Setup

### Coordinates

#### Maven

Repository
```xml
<repository>
    <id>gitlab-tfbr-minecommons</id>
    <url>https://gitlab.com/api/v4/projects/27141550/packages/maven</url>
</repository>
```
Dependency
```xml
<dependency>
    <groupId>com.gitlab.aecsocket.tfbr</groupId>
    <artifactId>[MODULE]</artifactId>
    <version>[VERSION]</version>
</dependency>
```

#### Gradle

Repository
```kotlin
maven("https://gitlab.com/api/v4/projects/27141550/packages/maven")
```

Dependency
```kotlin
implementation("com.gitlab.aecsocket.tfbr", "[MODULE]", "[VERSION]")
```

### Usage

#### [Javadoc](https://aecsocket.gitlab.io/tfbr)

### Modules

Implementations:
* Paper `paper`
