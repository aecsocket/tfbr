package com.gitlab.aecsocket.tfbr.endpoint;

import org.bukkit.entity.Entity;
import org.checkerframework.checker.nullness.qual.Nullable;

public interface EntityEndpoint extends Endpoint {
    Entity entity();

    void frequency(@Nullable Double frequency);
    void channel(@Nullable String channel);
}
