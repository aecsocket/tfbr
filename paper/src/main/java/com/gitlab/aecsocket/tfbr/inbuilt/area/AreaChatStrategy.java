package com.gitlab.aecsocket.tfbr.inbuilt.area;

import com.gitlab.aecsocket.tfbr.TFBRPlugin;
import com.gitlab.aecsocket.tfbr.endpoint.Endpoint;
import com.gitlab.aecsocket.tfbr.strategy.ChatStrategy;
import io.papermc.paper.event.player.AsyncChatEvent;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.objectmapping.ConfigSerializable;
import org.spongepowered.configurate.objectmapping.meta.Required;
import org.spongepowered.configurate.objectmapping.meta.Setting;

import static com.gitlab.aecsocket.tfbr.strategy.ChatStrategyUtils.*;

@ConfigSerializable
public record AreaChatStrategy(
        @Nullable String permission,
        @Required @Setting(nodeFromParent = true) AreaChatMessage.Options options
) implements ChatStrategy {
    public static final String ID = "area_chat";

    @Override
    public boolean applies(AsyncChatEvent event) {
        return hasPermission(event, permission);
    }

    @Override
    public void handle(TFBRPlugin plugin, AsyncChatEvent event, String message) {
        removeViewers(event);
        Endpoint.player(plugin, event.getPlayer()).transmit(plugin, (tx, rx) ->
                new AreaChatMessage(plugin, new AreaChatMessage.Options(options), tx, rx, message));
    }
}
