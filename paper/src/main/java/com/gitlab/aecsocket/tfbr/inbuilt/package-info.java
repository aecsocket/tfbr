/**
 * Inbuilt strategies and message types.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.tfbr.inbuilt;
