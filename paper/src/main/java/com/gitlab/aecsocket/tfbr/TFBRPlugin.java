package com.gitlab.aecsocket.tfbr;

import com.gitlab.aecsocket.minecommons.core.CollectionBuilder;
import com.gitlab.aecsocket.minecommons.paper.plugin.BaseCommand;
import com.gitlab.aecsocket.minecommons.paper.plugin.BasePlugin;
import com.gitlab.aecsocket.minecommons.paper.scheduler.PaperScheduler;
import com.gitlab.aecsocket.sokol.paper.SokolPlugin;
import com.gitlab.aecsocket.tfbr.endpoint.Endpoint;
import com.gitlab.aecsocket.tfbr.endpoint.EndpointCollectEvent;
import com.gitlab.aecsocket.tfbr.inbuilt.EquipmentChatStrategy;
import com.gitlab.aecsocket.tfbr.inbuilt.PassthroughStrategy;
import com.gitlab.aecsocket.tfbr.inbuilt.area.AreaChatStrategy;
import com.gitlab.aecsocket.tfbr.inbuilt.radio.RadioChatStrategy;
import com.gitlab.aecsocket.tfbr.strategy.*;
import com.gitlab.aecsocket.tfbr.system.RadioSystem;
import io.leangen.geantyref.TypeToken;
import org.bstats.bukkit.Metrics;
import org.bukkit.Bukkit;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.objectmapping.ObjectMapper;
import org.spongepowered.configurate.serialize.TypeSerializerCollection;

import java.util.*;

/**
 * TFBR's main plugin class.
 */
public class TFBRPlugin extends BasePlugin<TFBRPlugin> {
    /** The ID for this plugin on https://bstats.org. */
    public static final int BSTATS_ID = 11917;
    public static final String PERMISSION_INTERCEPT = "tfbr.intercept";

    private final PaperScheduler scheduler = new PaperScheduler(this);
    private final Noise noise = new Noise(this);
    private final Frequencies frequencies = new Frequencies(this);
    private final Map<String, Class<? extends ChatStrategy>> strategyTypes = CollectionBuilder.map(new HashMap<String, Class<? extends ChatStrategy>>())
            .put(PassthroughStrategy.ID, PassthroughStrategy.class)
            .put(AreaChatStrategy.ID, AreaChatStrategy.class)
            .put(RadioChatStrategy.ID, RadioChatStrategy.class)
            .put(EquipmentChatStrategy.ID, EquipmentChatStrategy.class)
            .get();
    private final Map<Character, Collection<ChatStrategy>> strategies = new HashMap<>();

    @Override
    public void onEnable() {
        super.onEnable();
        Bukkit.getPluginManager().registerEvents(new TFBRListener(this), this);
        SokolPlugin sokol = SokolPlugin.instance();
        sokol
                .registerSystemType(RadioSystem.ID, RadioSystem.type(sokol, this));
    }

    @Override
    protected void configOptionsDefaults(TypeSerializerCollection.Builder serializers, ObjectMapper.Factory.Builder mapperFactory) {
        super.configOptionsDefaults(serializers, mapperFactory);
        serializers.registerExact(ChatStrategy.class, new ChatStrategy.Serializer(this));
    }

    public PaperScheduler scheduler() { return scheduler; }
    public Noise noise() { return noise; }
    public Frequencies frequencies() { return frequencies; }
    public Map<String, Class<? extends ChatStrategy>> strategyTypes() { return strategyTypes; }
    public Map<Character, Collection<ChatStrategy>> strategies() { return strategies; }

    @Override
    public void load() {
        super.load();
        if (setting(true, ConfigurationNode::getBoolean, "enable_bstats")) {
            Metrics metrics = new Metrics(this, BSTATS_ID);
        }

        noise.load();
        frequencies.load();

        var strategies = setting(Collections.<String, Set<ChatStrategy>>emptyMap(), (n, d) -> n.get(new TypeToken<>() {}, d), "strategies");
        this.strategies.clear();
        for (var entry : strategies.entrySet()) {
            String key = entry.getKey();
            this.strategies.put(key.length() == 0 ? null : key.charAt(0), entry.getValue());
        }
    }

    public Set<Endpoint> collectEndpoints() {
        Set<Endpoint> endpoints = new HashSet<>();
        new EndpointCollectEvent(endpoints).callEvent();
        return endpoints;
    }

    @Override
    protected BaseCommand<TFBRPlugin> createCommand() throws Exception {
        return new TFBRCommand(this);
    }
}
