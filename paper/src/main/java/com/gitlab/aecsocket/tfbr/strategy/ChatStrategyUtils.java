package com.gitlab.aecsocket.tfbr.strategy;

import com.gitlab.aecsocket.tfbr.TFBRPlugin;
import io.papermc.paper.event.player.AsyncChatEvent;
import org.bukkit.permissions.Permissible;
import org.checkerframework.checker.nullness.qual.Nullable;

public final class ChatStrategyUtils {
    private ChatStrategyUtils() {}

    public static boolean hasPermission(AsyncChatEvent event, @Nullable String permission) {
        return permission == null || event.getPlayer().hasPermission(permission);
    }

    public static void removeViewers(AsyncChatEvent event) {
        event.viewers().remove(event.getPlayer());
        event.viewers().removeIf(aud -> !(aud instanceof Permissible permissible) || !permissible.hasPermission(TFBRPlugin.PERMISSION_INTERCEPT));
    }
}
