package com.gitlab.aecsocket.tfbr.endpoint;

import com.gitlab.aecsocket.tfbr.message.Message;
import net.kyori.adventure.text.Component;
import org.bukkit.Location;

import java.util.Locale;
import java.util.Optional;

public abstract class ForwardingEndpoint implements Endpoint {
    protected abstract Endpoint target();

    @Override public Component name() { return target().name(); }
    @Override public int code() { return target().code(); }
    @Override public Locale locale() { return target().locale(); }
    @Override public Location location() { return target().location(); }

    @Override public void receive(Message<?> message) { target().receive(message); }

    @Override public Optional<Double> frequency() { return target().frequency(); }
    @Override public Optional<String> channel() { return target().channel(); }
}
