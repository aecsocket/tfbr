package com.gitlab.aecsocket.tfbr.endpoint;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

import java.util.Set;

public class EndpointCollectEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private final Set<Endpoint> endpoints;

    public EndpointCollectEvent(Set<Endpoint> endpoints) {
        super(true);
        this.endpoints = endpoints;
    }

    public Set<Endpoint> endpoints() { return endpoints; }

    @Override public @NotNull HandlerList getHandlers() { return handlers; }
    public static @NotNull HandlerList getHandlerList() { return handlers; }
}
