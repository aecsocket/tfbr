package com.gitlab.aecsocket.tfbr.util;

import java.util.Optional;

public interface ReceivesFrequency {
    Optional<Double> frequency();
}
