package com.gitlab.aecsocket.tfbr;

import com.gitlab.aecsocket.minecommons.core.Validation;
import org.spongepowered.configurate.objectmapping.ConfigSerializable;

public final class Frequencies {
    @ConfigSerializable
    public static final class Config {
        public static final Config DEFAULT = new Config();

        public final double deltaStart = 0.02;
        public final double deltaEnd = 0.08;

        public final double powerNoiseStart = 0.25;

        private Config() {}

        private void validate() {
            Validation.in("deltaStart", deltaStart, 0, deltaEnd);
            Validation.greaterThanEquals("deltaEnd", deltaEnd, deltaStart);
            Validation.in("powerNoiseStart", powerNoiseStart, 0, 1);
        }
    }

    private final TFBRPlugin plugin;
    private Config config;

    Frequencies(TFBRPlugin plugin) {
        this.plugin = plugin;
    }

    public TFBRPlugin plugin() { return plugin; }
    public Config config() { return config; }

    void load() {
        config = plugin.setting(Config.DEFAULT, (n, d) -> n.get(Config.class, d), "frequencies");
        config.validate();
    }

    public double deltaNoise(double f1, double f2) {
        double delta = Math.abs(f2 - f1);
        return Math.max(0, (delta-config.deltaStart) / (config.deltaEnd-config.deltaStart));
    }

    public double distanceNoise(double distanceSqr, double powerSqr) {
        double percent = distanceSqr / powerSqr;
        double max = 1 - config.powerNoiseStart;
        return Math.max(0, (percent - config.powerNoiseStart) / max);
    }
}
