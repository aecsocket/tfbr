package com.gitlab.aecsocket.tfbr.endpoint;

import com.gitlab.aecsocket.tfbr.Noise;
import com.gitlab.aecsocket.tfbr.TFBRPlugin;
import com.gitlab.aecsocket.tfbr.inbuilt.area.AreaChatMessage;
import com.gitlab.aecsocket.tfbr.inbuilt.radio.RadioMessage;
import com.gitlab.aecsocket.tfbr.message.Message;
import com.gitlab.aecsocket.tfbr.message.ChatMessage;
import com.gitlab.aecsocket.tfbr.util.Frequency;
import org.bukkit.entity.Player;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Locale;

/* package */ class PlayerEndpointImpl extends LivingEntityEndpointImpl implements PlayerEndpoint {
    private static final DecimalFormat format = new DecimalFormat("0.###");

    private final Player entity;

    public PlayerEndpointImpl(TFBRPlugin plugin, Player entity) {
        super(plugin, entity);
        this.entity = entity;
    }

    @Override public Player entity() { return entity; }

    @Override public Locale locale() { return entity.locale(); }

    private void receiveChat(ChatMessage<?> chat, double noise, Object... args) {
        int l = args.length;
        Object[] newArgs = Arrays.copyOf(args, args.length + 6);
        newArgs[l] = "tx";
        newArgs[l+1] = chat.options().obfuscation().map(chat.transmitter());

        newArgs[l+2] = "tx_name";
        newArgs[l+3] = chat.transmitter().name();

        newArgs[l+4] = "content";
        newArgs[l+5] = plugin().noise().apply(chat.content(), noise);

        plugin().lc().get(locale(), chat.options().chatKey() + (noise < 0 ? ".sending" : ".receiving"), newArgs)
                .ifPresent(c -> chat.options().position().send(entity, c));
    }

    @Override
    public void receive(Message<?> message) {
        double noise = message.noise();
        if (noise >= 1)
            return;
        double distance = message.transmitter().location().distance(message.receiver().location());

        if (message instanceof RadioMessage radio) {
            receiveChat(radio, noise,
                    "rx_frequency", Frequency.of(frequency().orElseThrow(() ->
                            new IllegalStateException("Cannot receive radio message with no frequency set"))).name(plugin(), locale()),
                    "tx_frequency", Frequency.of(radio.options().frequency()).name(plugin(), locale()),
                    "distance", format.format(Noise.round(distance, radio.options().distanceRounding())));
            return;
        }
        if (message instanceof AreaChatMessage chat) {
            receiveChat(chat, noise,
                    "distance", format.format(Noise.round(distance, chat.options().distanceRounding())));
            return;
        }
        if (message instanceof ChatMessage<?> chat) {
            receiveChat(chat, noise);
        }
    }
}
