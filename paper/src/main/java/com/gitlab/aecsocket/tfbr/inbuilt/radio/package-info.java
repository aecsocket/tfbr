/**
 * Inbuilt strategies and message types - radio chat.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.tfbr.inbuilt.radio;
