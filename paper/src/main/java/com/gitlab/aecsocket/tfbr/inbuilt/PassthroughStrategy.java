package com.gitlab.aecsocket.tfbr.inbuilt;

import com.gitlab.aecsocket.tfbr.TFBRPlugin;
import com.gitlab.aecsocket.tfbr.strategy.ChatStrategy;
import io.papermc.paper.event.player.AsyncChatEvent;
import net.kyori.adventure.text.Component;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.objectmapping.ConfigSerializable;

import static com.gitlab.aecsocket.tfbr.strategy.ChatStrategyUtils.hasPermission;

@ConfigSerializable
public record PassthroughStrategy(
        @Nullable String permission
) implements ChatStrategy {
    public static final String ID = "passthrough";

    @Override
    public boolean applies(AsyncChatEvent event) {
        return hasPermission(event, permission);
    }

    @Override
    public void handle(TFBRPlugin plugin, AsyncChatEvent event, String message) {
        event.message(Component.text(message));
    }
}
