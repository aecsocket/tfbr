/**
 * Messages which carry content and metadata of a transmission.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.tfbr.message;
