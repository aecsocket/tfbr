package com.gitlab.aecsocket.tfbr.inbuilt;

import com.gitlab.aecsocket.sokol.paper.SokolPlugin;
import com.gitlab.aecsocket.tfbr.TFBRPlugin;
import com.gitlab.aecsocket.tfbr.endpoint.Endpoint;
import com.gitlab.aecsocket.tfbr.endpoint.EquipmentEndpoint;
import com.gitlab.aecsocket.tfbr.endpoint.PlayerEndpoint;
import com.gitlab.aecsocket.tfbr.strategy.ChatStrategy;
import io.papermc.paper.event.player.AsyncChatEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.PlayerInventory;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.objectmapping.ConfigSerializable;
import org.spongepowered.configurate.objectmapping.meta.Required;
import org.spongepowered.configurate.objectmapping.meta.Setting;

import static com.gitlab.aecsocket.tfbr.strategy.ChatStrategyUtils.hasPermission;
import static com.gitlab.aecsocket.tfbr.strategy.ChatStrategyUtils.removeViewers;

@ConfigSerializable
public record EquipmentChatStrategy(
        @Nullable String permission,
        @Required EquipmentSlot slot
) implements ChatStrategy {
    public static final String ID = "equipment_chat";

    @Override
    public boolean applies(AsyncChatEvent event) {
        return hasPermission(event, permission);
    }

    @Override
    public void handle(TFBRPlugin plugin, AsyncChatEvent event, String message) {
        removeViewers(event);
        PlayerInventory inv = event.getPlayer().getInventory();
        PlayerEndpoint playerEndpoint = Endpoint.player(plugin, event.getPlayer());
        for (EquipmentSlot slot : EquipmentSlot.values()) {
            SokolPlugin.instance().persistenceManager().load(inv.getItem(slot)).ifPresent(node -> {
                EquipmentEndpoint endpoint = Endpoint.equipment(playerEndpoint, slot, node);
                node.events().call(new EquipmentEndpoint.Events.Transmit(node, endpoint, message));
            });
        }
    }
}
