package com.gitlab.aecsocket.tfbr.util;

import java.util.Optional;

public interface ReceivesChannel {
    Optional<String> channel();
}
