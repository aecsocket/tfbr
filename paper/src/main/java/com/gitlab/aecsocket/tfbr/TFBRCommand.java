package com.gitlab.aecsocket.tfbr;

import cloud.commandframework.ArgumentDescription;
import cloud.commandframework.bukkit.parsers.PlayerArgument;
import cloud.commandframework.captions.SimpleCaptionRegistry;
import cloud.commandframework.context.CommandContext;
import com.gitlab.aecsocket.minecommons.paper.plugin.BaseCommand;
import com.gitlab.aecsocket.tfbr.endpoint.Endpoint;
import com.gitlab.aecsocket.tfbr.util.Frequency;
import com.gitlab.aecsocket.tfbr.util.FrequencyArgument;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Locale;

/* package */ class TFBRCommand extends BaseCommand<TFBRPlugin> {
    public TFBRCommand(TFBRPlugin plugin) throws Exception {
        super(plugin, "tfbr",
                (mgr, root) -> mgr.commandBuilder(root, ArgumentDescription.of("Plugin main command.")));

        SimpleCaptionRegistry<CommandSender> captions = (SimpleCaptionRegistry<CommandSender>) manager.getCaptionRegistry();
        captions.registerMessageFactory(FrequencyArgument.ARGUMENT_PARSE_FAILURE_FREQUENCY, (c, s) -> "Invalid frequency `{input}`: {error}");

        var fqRoot = root.literal("fq",
                ArgumentDescription.of("Frequency commands."));

        manager.command(fqRoot
                .literal("get", ArgumentDescription.of("Gets a player's personal frequency."))
                .argument(PlayerArgument.optional("target"), ArgumentDescription.of("The target player."))
                .permission("%s.command.fq.get".formatted(rootName))
                .handler(c -> handle(c, this::fqGet)));
        manager.command(fqRoot
                .literal("set", ArgumentDescription.of("Sets a player's personal frequency."))
                .argument(FrequencyArgument.of("frequency"), ArgumentDescription.of("The frequency to set to."))
                .argument(PlayerArgument.optional("target"), ArgumentDescription.of("The target player."))
                .permission("%s.command.fq.set".formatted(rootName))
                .handler(c -> handle(c, this::fqSet)));
        manager.command(fqRoot
                .literal("unset", ArgumentDescription.of("Unsets a player's personal frequency."))
                .argument(PlayerArgument.optional("target"), ArgumentDescription.of("The target player."))
                .permission("%s.command.fq.unset".formatted(rootName))
                .handler(c -> handle(c, this::fqUnset)));
    }

    private void fqGet(CommandContext<CommandSender> ctx, CommandSender sender, Locale locale, Player pSender) {
        Player target = defaultedArg(ctx, "target", pSender, () -> pSender);
        if (!sender.hasPermission("%s.command.fq.get.others".formatted(rootName)) && target != pSender)
            throw error("permission_others");
        Endpoint.player(plugin, target).frequency().ifPresentOrElse(
                freq -> send(sender, locale, "fq.get.set",
                                "target", target.displayName(),
                                "frequency", Frequency.of(freq).name(plugin, locale)),
                () -> send(sender, locale, "fq.get.unset",
                        "target", target.displayName()));
    }

    private void fqSet(CommandContext<CommandSender> ctx, CommandSender sender, Locale locale, Player pSender) {
        Player target = defaultedArg(ctx, "target", pSender, () -> pSender);
        if (!sender.hasPermission("%s.command.fq.set.others".formatted(rootName)) && target != pSender)
            throw error("permission_others");

        Frequency frequency = ctx.get("frequency");
        Endpoint.player(plugin, target).frequency(frequency.raw());
        send(sender, locale, "fq.set",
                "target", target.displayName(),
                "frequency", frequency.name(plugin, locale));
    }

    private void fqUnset(CommandContext<CommandSender> ctx, CommandSender sender, Locale locale, Player pSender) {
        Player target = defaultedArg(ctx, "target", pSender, () -> pSender);
        if (!sender.hasPermission("%s.command.fq.unset.others".formatted(rootName)) && target != pSender)
            throw error("permission_others");

        Endpoint.player(plugin, target).frequency(null);
        send(sender, locale, "fq.unset",
                "target", target.displayName());
    }
}
