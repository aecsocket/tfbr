package com.gitlab.aecsocket.tfbr;

import com.gitlab.aecsocket.minecommons.core.Numbers;
import com.gitlab.aecsocket.minecommons.core.Validation;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.TextDecoration;
import org.spongepowered.configurate.objectmapping.ConfigSerializable;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public final class Noise {
    @ConfigSerializable
    public static final class Config {
        public static final Config DEFAULT = new Config();

        public final double min = 0;
        public final double max = 1;
        public final Component placeholder = Component.text(" ").decorate(TextDecoration.STRIKETHROUGH);

        private Config() {}

        private void validate() {
            Validation.in("min", min, 0, max);
            Validation.in("max", max, min, 1);
        }
    }

    private final TFBRPlugin plugin;
    private Config config;

    Noise(TFBRPlugin plugin) {
        this.plugin = plugin;
    }

    public TFBRPlugin plugin() { return plugin; }
    public Config config() { return config; }

    void load() {
        config = plugin.setting(Config.DEFAULT, (n, d) -> n.get(Config.class, d), "noise");
        config.validate();
    }

    public Component apply(String msg, double noise) {
        noise = Numbers.clamp(noise, config.min, config.max);
        Random rng = ThreadLocalRandom.current();
        Component[] result = new Component[msg.length()];
        for (int i = 0; i < msg.length(); i++) {
            result[i] = rng.nextDouble() < noise
                    ? config.placeholder
                    : Component.text(msg.charAt(i));
        }
        return Component.text().append(result).build();
    }

    public static double round(double val, double rounding) {
        return ((int) (val / rounding)) * rounding;
    }
}
