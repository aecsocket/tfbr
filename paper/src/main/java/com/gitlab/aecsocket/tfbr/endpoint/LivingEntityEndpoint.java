package com.gitlab.aecsocket.tfbr.endpoint;

import org.bukkit.entity.LivingEntity;

public interface LivingEntityEndpoint extends EntityEndpoint {
    LivingEntity entity();
}
