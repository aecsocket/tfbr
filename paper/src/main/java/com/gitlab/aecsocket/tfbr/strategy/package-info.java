/**
 * Chat strategies which determine how a chat event is handled.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.tfbr.strategy;
