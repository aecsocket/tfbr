package com.gitlab.aecsocket.tfbr.strategy;

import com.gitlab.aecsocket.minecommons.core.serializers.Serializers;
import com.gitlab.aecsocket.tfbr.TFBRPlugin;
import io.papermc.paper.event.player.AsyncChatEvent;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;
import org.spongepowered.configurate.serialize.TypeSerializer;

import java.lang.reflect.Type;

public interface ChatStrategy {
    final class Serializer implements TypeSerializer<ChatStrategy> {
        private final TFBRPlugin plugin;

        public Serializer(TFBRPlugin plugin) {
            this.plugin = plugin;
        }

        public TFBRPlugin plugin() { return plugin; }

        @Override
        public void serialize(Type type, @Nullable ChatStrategy obj, ConfigurationNode node) throws SerializationException {
            node.set(obj);
        }

        @Override
        public ChatStrategy deserialize(Type type, ConfigurationNode node) throws SerializationException {
            String strategyType = Serializers.require(node.node("type"), String.class);
            Class<? extends ChatStrategy> clazz = plugin.strategyTypes().get(strategyType);
            if (clazz == null)
                throw new SerializationException(node, type, "Invalid strategy type [" + strategyType + "]");
            return node.get(clazz);
        }
    }

    boolean applies(AsyncChatEvent event);

    void handle(TFBRPlugin plugin, AsyncChatEvent event, String message);
}
