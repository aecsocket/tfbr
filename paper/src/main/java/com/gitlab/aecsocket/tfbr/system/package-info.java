/**
 * Sokol item systems for TFBR.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.tfbr.system;
