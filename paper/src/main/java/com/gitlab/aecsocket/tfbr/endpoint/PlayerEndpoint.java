package com.gitlab.aecsocket.tfbr.endpoint;

import org.bukkit.entity.Player;

public interface PlayerEndpoint extends LivingEntityEndpoint {
    Player entity();
}
