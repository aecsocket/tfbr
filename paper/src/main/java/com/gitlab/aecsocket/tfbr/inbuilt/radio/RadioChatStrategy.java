package com.gitlab.aecsocket.tfbr.inbuilt.radio;

import com.gitlab.aecsocket.minecommons.core.ChatPosition;
import com.gitlab.aecsocket.tfbr.TFBRPlugin;
import com.gitlab.aecsocket.tfbr.endpoint.Endpoint;
import com.gitlab.aecsocket.tfbr.endpoint.PlayerEndpoint;
import com.gitlab.aecsocket.tfbr.strategy.ChatStrategy;
import io.papermc.paper.event.player.AsyncChatEvent;
import org.bukkit.entity.Player;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.objectmapping.ConfigSerializable;
import org.spongepowered.configurate.objectmapping.meta.Required;
import org.spongepowered.configurate.objectmapping.meta.Setting;

import static com.gitlab.aecsocket.tfbr.strategy.ChatStrategyUtils.hasPermission;
import static com.gitlab.aecsocket.tfbr.strategy.ChatStrategyUtils.removeViewers;

@ConfigSerializable
public record RadioChatStrategy(
        @Nullable String permission,
        @Nullable String unsetKey,
        @Nullable ChatPosition unsetPosition,
        @Required @Setting(nodeFromParent = true) RadioMessage.Options options
) implements ChatStrategy {
    public static final String ID = "radio_chat";

    @Override
    public boolean applies(AsyncChatEvent event) {
        return hasPermission(event, permission);
    }

    @Override
    public void handle(TFBRPlugin plugin, AsyncChatEvent event, String message) {
        removeViewers(event);
        Player player = event.getPlayer();
        PlayerEndpoint endpoint = Endpoint.player(plugin, player);
        endpoint.frequency().ifPresentOrElse(
                freq -> {
                    RadioMessage.Options options = new RadioMessage.Options(this.options);
                    options.frequency = freq;
                    endpoint.transmit(plugin, (tx, rx) ->
                            new RadioMessage(plugin, options, tx, rx, message));
                },
                () -> {
                    if (unsetKey != null && unsetPosition != null) {
                        plugin.lc().get(player.locale(), unsetKey).ifPresent(c ->
                                unsetPosition.send(player, c));
                    }
                }
        );
    }
}
