package com.gitlab.aecsocket.tfbr.inbuilt.area;

import com.gitlab.aecsocket.minecommons.core.ChatPosition;
import com.gitlab.aecsocket.tfbr.TFBRPlugin;
import com.gitlab.aecsocket.tfbr.endpoint.Endpoint;
import com.gitlab.aecsocket.tfbr.message.ChatMessage;
import com.gitlab.aecsocket.tfbr.message.Obfuscation;
import org.spongepowered.configurate.objectmapping.ConfigSerializable;
import org.spongepowered.configurate.objectmapping.meta.Required;

public record AreaChatMessage(
        TFBRPlugin plugin,
        Options options,
        Endpoint transmitter, Endpoint receiver,
        String content
) implements ChatMessage<AreaChatMessage.Options> {
    @ConfigSerializable
    public static class Options extends ChatMessage.Options {
        protected @Required double dropoff;
        protected @Required double range;
        protected double distanceRounding;

        public Options(Obfuscation obfuscation, String chatKey, ChatPosition position, double dropoff, double range, double distanceRounding) {
            super(obfuscation, chatKey, position);
            this.dropoff = dropoff;
            this.range = range;
            this.distanceRounding = distanceRounding;
        }

        public Options(Options o) {
            super(o);
            dropoff = o.dropoff;
            range = o.range;
            distanceRounding = o.distanceRounding;
        }

        private Options() { this(Obfuscation.NONE, "", ChatPosition.CHAT, 0, 0, 1); }

        public double dropoff() { return dropoff; }
        public void dropoff(double dropoff) { this.dropoff = dropoff; }

        public double range() { return range; }
        public void range(double range) { this.range = range; }

        public double distanceRounding() { return distanceRounding; }
        public void distanceRounding(double distanceRounding) { this.distanceRounding = distanceRounding; }
    }

    @Override
    public double noise() {
        if (transmitter.equals(receiver))
            return -1;
        double dst = transmitter.location().distanceSquared(receiver.location());
        double dropoff = options.dropoff * options.dropoff;
        double range = options.range * options.range;
        return Math.max(0, (dst-dropoff) / (range-dropoff));
    }
}
