package com.gitlab.aecsocket.tfbr.endpoint;

import com.gitlab.aecsocket.sokol.paper.PaperTreeNode;
import com.gitlab.aecsocket.tfbr.message.Message;
import org.bukkit.inventory.EquipmentSlot;

import java.util.Optional;

/* package */ class EquipmentEndpointImpl extends ForwardingEndpoint implements EquipmentEndpoint {
    private final LivingEntityEndpoint entity;
    private final EquipmentSlot slot;
    private final PaperTreeNode node;

    public EquipmentEndpointImpl(LivingEntityEndpoint entity, EquipmentSlot slot, PaperTreeNode node) {
        this.entity = entity;
        this.slot = slot;
        this.node = node;
    }

    @Override protected Endpoint target() { return entity; }
    @Override public LivingEntityEndpoint entity() { return entity; }
    @Override public EquipmentSlot slot() { return slot; }
    @Override public PaperTreeNode node() { return node; }

    @Override
    public void receive(Message<?> message) {
        node.events().call(new Events.Receive(node, this, message));
    }

    @Override
    public Optional<Double> frequency() {
        Events.Callback<Double> callback = new Events.Callback<>();
        node.events().call(new Events.GetFrequency(node, this, callback));
        return Optional.ofNullable(callback.get());
    }

    @Override
    public Optional<String> channel() {
        Events.Callback<String> callback = new Events.Callback<>();
        node.events().call(new Events.GetChannel(node, this, callback));
        return callback.optional();
    }
}
