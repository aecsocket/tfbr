/**
 * Inbuilt strategies and message types - area chat.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.tfbr.inbuilt.area;
