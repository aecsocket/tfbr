package com.gitlab.aecsocket.tfbr.message;

import com.gitlab.aecsocket.tfbr.endpoint.Endpoint;

public interface Message<O extends Message.Options> {
    abstract class Options {
        protected Obfuscation obfuscation;

        public Options(Obfuscation obfuscation) {
            this.obfuscation = obfuscation;
        }

        public Options(Options o) {
            obfuscation = o.obfuscation;
        }

        private Options() { this(Obfuscation.NONE); }

        public Obfuscation obfuscation() { return obfuscation; }
        public void obfuscation(Obfuscation obfuscation) { this.obfuscation = obfuscation; }
    }

    O options();

    Endpoint transmitter();
    Endpoint receiver();

    /*
    0 < noise < 1: the receiver is being sent the message
    noise = 0: the receiver is the transmitter
    noise >= 1: the receiver cannot hear the message
     */
    double noise();
}
