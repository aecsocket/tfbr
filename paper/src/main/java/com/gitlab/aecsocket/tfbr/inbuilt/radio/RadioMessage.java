package com.gitlab.aecsocket.tfbr.inbuilt.radio;

import com.gitlab.aecsocket.minecommons.core.ChatPosition;
import com.gitlab.aecsocket.tfbr.Frequencies;
import com.gitlab.aecsocket.tfbr.TFBRPlugin;
import com.gitlab.aecsocket.tfbr.endpoint.Endpoint;
import com.gitlab.aecsocket.tfbr.message.ChatMessage;
import com.gitlab.aecsocket.tfbr.message.Obfuscation;
import com.google.common.util.concurrent.AtomicDouble;
import org.spongepowered.configurate.objectmapping.ConfigSerializable;
import org.spongepowered.configurate.objectmapping.meta.Required;

public record RadioMessage(
        TFBRPlugin plugin,
        Options options,
        Endpoint transmitter, Endpoint receiver,
        String content
) implements ChatMessage<RadioMessage.Options> {
    @ConfigSerializable
    public static class Options extends ChatMessage.Options {
        protected double frequency;
        protected @Required double power;
        protected double distanceRounding;

        public Options(Obfuscation obfuscation, String chatKey, ChatPosition position, double frequency, double power, double distanceRounding) {
            super(obfuscation, chatKey, position);
            this.frequency = frequency;
            this.power = power;
            this.distanceRounding = distanceRounding;
        }

        public Options(Options o) {
            super(o);
            frequency = o.frequency;
            power = o.power;
            distanceRounding = o.distanceRounding;
        }

        private Options() { this(Obfuscation.NONE, "", ChatPosition.CHAT, 0, 0, 1); }

        public double frequency() { return frequency; }
        public void frequency(double frequency) { this.frequency = frequency; }

        public double power() { return power; }
        public void power(double power) { this.power = power; }

        public double distanceRounding() { return distanceRounding; }
        public void distanceRounding(double distanceRounding) { this.distanceRounding = distanceRounding; }
    }

    @Override
    public double noise() {
        if (transmitter.equals(receiver))
            return -1;
        AtomicDouble result = new AtomicDouble(1);
        receiver.frequency().ifPresent(freq -> {
            // TODO calculate noise
            Frequencies freqs = plugin.frequencies();
            result.set(
                    freqs.deltaNoise(options.frequency, freq)
                    + freqs.distanceNoise(transmitter.location().distanceSquared(receiver.location()), options.power * options.power)
            );
        });
        return result.get();
    }
}
