package com.gitlab.aecsocket.tfbr.endpoint;

import com.gitlab.aecsocket.sokol.core.tree.event.TreeEvent;
import com.gitlab.aecsocket.sokol.paper.PaperTreeNode;
import com.gitlab.aecsocket.tfbr.message.Message;
import org.bukkit.inventory.EquipmentSlot;

import java.util.Optional;

public interface EquipmentEndpoint extends Endpoint {
    LivingEntityEndpoint entity();
    EquipmentSlot slot();

    PaperTreeNode node();

    final class Events {
        private Events() {}

        public static final class Callback<T> {
            private T value;

            public T get() { return value; }
            public void set(T value) { this.value = value; }

            public Optional<T> optional() {
                return Optional.ofNullable(value);
            }
        }

        public static record Transmit(
                PaperTreeNode node,
                EquipmentEndpoint endpoint,
                String content
        ) implements TreeEvent {}

        public static record Receive(
                PaperTreeNode node,
                EquipmentEndpoint endpoint,
                Message<?> message
        ) implements TreeEvent {}

        public static record GetFrequency(
                PaperTreeNode node,
                EquipmentEndpoint endpoint,
                Callback<Double> frequency
        ) implements TreeEvent {}

        public static record GetChannel(
                PaperTreeNode node,
                EquipmentEndpoint endpoint,
                Callback<String> channel
        ) implements TreeEvent {}
    }
}
