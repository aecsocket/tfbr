package com.gitlab.aecsocket.tfbr.util;

import com.gitlab.aecsocket.minecommons.core.Validation;
import com.gitlab.aecsocket.tfbr.TFBRPlugin;
import net.kyori.adventure.text.Component;

import java.text.DecimalFormat;
import java.util.*;

// 476MHz :)
public record Frequency(double value, Unit unit) {
    private static final DecimalFormat format = new DecimalFormat("0.###");

    public enum Unit {
        HZ  ("hz", 1e-7),
        KHZ ("khz", 1e-3),
        MHZ ("mhz", 1),
        GHZ ("ghz", 1e3);

        private static final Map<String, Unit> byKey = new HashMap<>();
        private static final List<Unit> byLength = Arrays.asList(values());
        public static final Unit DEFAULT = MHZ;
        public static final Unit LARGEST = GHZ;

        static {
            for (Unit unit : values()) {
                byKey.put(unit.key, unit);
            }
            byLength.sort(Comparator.comparingInt(u -> -u.key.length()));
        }

        private final String key;
        private final double toDefault;
        private final double fromDefault;

        Unit(String key, double toDefault) {
            this.key = key;
            this.toDefault = toDefault;
            fromDefault = 1 / toDefault;
        }

        public static Unit nearest(double value) {
            for (Unit unit : values()) {
                if (unit.convert(value, DEFAULT) < 1000)
                    return unit;
            }
            return LARGEST;
        }

        public static Unit byKey(String key) {
            return byKey.get(key);
        }

        public static Map<String, Unit> byKey() { return Collections.unmodifiableMap(byKey); }

        public String key() { return key; }
        public double toDefault() { return toDefault; }
        public double fromDefault() { return fromDefault; }

        public double convert(double sourceValue, Unit sourceUnit) {
            return sourceValue * (sourceUnit.toDefault / toDefault);
        }
    }

    public Frequency {
        Validation.greaterThan("value", value, 0);
        Validation.notNull("unit", unit);
    }

    public static Frequency of(double value) {
        Unit unit = Unit.nearest(value);
        return new Frequency(unit.convert(value, Unit.DEFAULT), unit);
    }

    public static Frequency of(String text) throws NumberFormatException {
        for (Unit unit : Unit.byLength) {
            String key = unit.key;
            if (text.toLowerCase(Locale.ROOT).endsWith(key)) {
                return new Frequency(
                        Double.parseDouble(text.substring(0, text.length() - key.length())),
                        unit
                );
            }
        }
        return of(Double.parseDouble(text));
    }

    public double raw() {
        return Unit.DEFAULT.convert(value, unit);
    }

    public Component name(TFBRPlugin plugin, Locale locale) {
        String key = "units.frequency." + unit.key;
        return plugin.lc().safe(locale, key,
                "v", format.format(value));
    }
}
