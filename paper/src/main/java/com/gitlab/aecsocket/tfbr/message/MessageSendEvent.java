package com.gitlab.aecsocket.tfbr.message;

import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

public class MessageSendEvent extends Event implements Cancellable {
    private static final HandlerList handlers = new HandlerList();
    private final Message<?> message;
    private boolean cancelled;

    public MessageSendEvent(Message<?> message) {
        super(true);
        this.message = message;
    }

    public Message<?> message() { return message; }

    @Override public boolean isCancelled() { return cancelled; }
    @Override public void setCancelled(boolean cancel) { cancelled = cancel; }

    @Override public @NotNull HandlerList getHandlers() { return handlers; }
    public static HandlerList getHandlerList() { return handlers; }
}
