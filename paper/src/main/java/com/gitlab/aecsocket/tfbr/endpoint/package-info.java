/**
 * A node in a message network which can send or receive a message.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.tfbr.endpoint;
