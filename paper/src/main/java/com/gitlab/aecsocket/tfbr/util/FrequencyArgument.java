package com.gitlab.aecsocket.tfbr.util;

import cloud.commandframework.ArgumentDescription;
import cloud.commandframework.arguments.CommandArgument;
import cloud.commandframework.arguments.parser.ArgumentParseResult;
import cloud.commandframework.arguments.parser.ArgumentParser;
import cloud.commandframework.captions.Caption;
import cloud.commandframework.captions.CaptionVariable;
import cloud.commandframework.context.CommandContext;
import cloud.commandframework.exceptions.parsing.NoInputProvidedException;
import cloud.commandframework.exceptions.parsing.ParserException;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.*;
import java.util.function.BiFunction;

/**
 * Command argument which parses a {@link Frequency}.
 * @param <C> The command sender type.
 */
public final class FrequencyArgument<C> extends CommandArgument<C, Frequency> {
    /** When a frequency cannot be parsed. */
    public static final Caption ARGUMENT_PARSE_FAILURE_FREQUENCY = Caption.of("argument.parse.failure.frequency");

    private FrequencyArgument(
            final boolean required,
            final @NonNull String name,
            final @NonNull String defaultValue,
            final @Nullable BiFunction<@NonNull CommandContext<C>,
                    @NonNull String, @NonNull List<@NonNull String>> suggestionsProvider,
            final @NonNull ArgumentDescription defaultDescription
    ) {
        super(required, name, new FrequencyParser<>(), defaultValue, Frequency.class, suggestionsProvider, defaultDescription);
    }

    /**
     * Create a new builder
     *
     * @param name   Name of the component
     * @param <C>    Command sender type
     * @return Created builder
     */
    public static <C> @NonNull Builder<C> newBuilder(final @NonNull String name) {
        return new Builder<>(name);
    }

    /**
     * Create a new required command component
     *
     * @param name   Component name
     * @param <C>    Command sender type
     * @return Created component
     */
    public static <C> @NonNull CommandArgument<C, Frequency> of(final @NonNull String name) {
        return FrequencyArgument.<C>newBuilder(name).asRequired().build();
    }

    /**
     * Create a new optional command component
     *
     * @param name   Component name
     * @param <C>    Command sender type
     * @return Created component
     */
    public static <C> @NonNull CommandArgument<C, Frequency> optional(final @NonNull String name) {
        return FrequencyArgument.<C>newBuilder(name).asOptional().build();
    }

    /**
     * Create a new required command component with a default value
     *
     * @param name         Component name
     * @param defaultValue Default value
     * @param <C>          Command sender type
     * @return Created component
     */
    public static <C> @NonNull CommandArgument<C, Frequency> optional(
            final @NonNull String name,
            final @NonNull Frequency defaultValue
    ) {
        return FrequencyArgument.<C>newBuilder(name).asOptionalWithDefault(defaultValue.toString()).build();
    }


    public static final class Builder<C> extends CommandArgument.Builder<C, Frequency> {
        private Builder(final @NonNull String name) {
            super(Frequency.class, name);
        }

        /**
         * Builder a new example component
         *
         * @return Constructed component
         */
        @Override
        public @NonNull FrequencyArgument<C> build() {
            return new FrequencyArgument<>(
                    this.isRequired(),
                    this.getName(),
                    this.getDefaultValue(),
                    this.getSuggestionsProvider(),
                    this.getDefaultDescription()
            );
        }

    }

    public static final class FrequencyParser<C> implements ArgumentParser<C, Frequency> {
        @Override
        public @NonNull ArgumentParseResult<Frequency> parse(
                final @NonNull CommandContext<C> ctx,
                final @NonNull Queue<@NonNull String> inputQueue
        ) {
            final String input = inputQueue.peek();
            if (input == null) {
                return ArgumentParseResult.failure(new NoInputProvidedException(
                        Frequency.class,
                        ctx
                ));
            }
            inputQueue.remove();

            try {
                return ArgumentParseResult.success(Frequency.of(input));
            } catch (IllegalArgumentException e) {
                return ArgumentParseResult.failure(new ParseException(input, ctx, e));
            }
        }

        @Override
        public boolean isContextFree() {
            return true;
        }

        @Override
        public @NonNull List<@NonNull String> suggestions(@NonNull CommandContext<C> ctx, @NonNull String input) {
            /*
            eg: 120mh
            `h`: hz? no, khz? no...
            `mh`: h? no, khz? no, mhz starts with `mh`? YES, substring (len - 2)
             */
            if (input.length() == 0)
                return Collections.singletonList("<number>(" + String.join("|", Frequency.Unit.byKey().keySet()) + ")");
            int len = input.length();
            for (int i = len - 1; i > 0; i--) {
                String sub = input.substring(i, len);
                for (Frequency.Unit unit : Frequency.Unit.values()) {
                    if (unit.key().startsWith(sub)) {
                        return Collections.singletonList(input.substring(0, i) + unit.key());
                    }
                }
            }

            List<String> results = new ArrayList<>();
            for (Frequency.Unit unit : Frequency.Unit.values()) {
                results.add(input + unit.key());
            }
            return results;
        }
    }

    public static final class ParseException extends ParserException {
        public ParseException(String input, CommandContext<?> ctx, Exception e) {
            super(Frequency.class, ctx, ARGUMENT_PARSE_FAILURE_FREQUENCY,
                    CaptionVariable.of("input", input),
                    CaptionVariable.of("error", e.toString()));
        }
    }
}
