package com.gitlab.aecsocket.tfbr.system;

import com.github.stefvanschie.inventoryframework.gui.type.AnvilGui;
import com.gitlab.aecsocket.minecommons.core.ChatPosition;
import com.gitlab.aecsocket.minecommons.core.CollectionBuilder;
import com.gitlab.aecsocket.minecommons.core.Components;
import com.gitlab.aecsocket.sokol.core.stat.Stat;
import com.gitlab.aecsocket.sokol.core.stat.inbuilt.NumberStat;
import com.gitlab.aecsocket.sokol.core.system.AbstractSystem;
import com.gitlab.aecsocket.sokol.core.system.ItemSystem;
import com.gitlab.aecsocket.sokol.core.system.util.InputMapper;
import com.gitlab.aecsocket.sokol.core.tree.TreeNode;
import com.gitlab.aecsocket.sokol.core.tree.event.ItemTreeEvent;
import com.gitlab.aecsocket.sokol.paper.Guis;
import com.gitlab.aecsocket.sokol.paper.PaperTreeNode;
import com.gitlab.aecsocket.sokol.paper.SokolPlugin;
import com.gitlab.aecsocket.sokol.paper.system.PaperSystem;
import com.gitlab.aecsocket.sokol.paper.wrapper.user.PlayerUser;
import com.gitlab.aecsocket.tfbr.TFBRPlugin;
import com.gitlab.aecsocket.tfbr.util.Frequency;
import net.kyori.adventure.text.Component;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.persistence.PersistentDataAdapterContext;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.configurate.ConfigurationNode;
import org.spongepowered.configurate.serialize.SerializationException;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;

import static net.kyori.adventure.text.Component.*;

public class RadioSystem extends AbstractSystem implements PaperSystem {
    public static final String ID = "radio";
    public static final String KEY_FREQUENCY = "frequency";
    public static final String KEY_CHANNEL = "channel";
    public static final Map<String, Stat<?>> STATS = CollectionBuilder.map(new HashMap<String, Stat<?>>())
            .put("min_frequency", NumberStat.ofDouble(0))
            .put("max_frequency", NumberStat.ofDouble(Double.MAX_VALUE))
            .put("power", NumberStat.ofDouble(0))
            .build();

    public final class Instance extends AbstractSystem.Instance implements PaperSystem.Instance {
        private @Nullable Double frequency;
        private @Nullable String channel;

        public Instance(TreeNode parent) {
            super(parent);
        }

        @Override public RadioSystem base() { return RadioSystem.this; }
        @Override public SokolPlugin platform() { return platform; }

        public Optional<Double> frequency() { return Optional.ofNullable(frequency); }
        public void frequency(Double frequency) { this.frequency = frequency; }

        public Optional<String> channel() { return Optional.ofNullable(channel); }
        public void channel(String channel) { this.channel = channel; }

        @Override
        public void build() {
            parent.events().register(ItemSystem.Events.CreateItem.class, this::event);
            parent.events().register(ItemTreeEvent.InputEvent.class, this::event);
        }

        public Component channelName(Locale locale, String channel) {
            return tfbr.lc().get(locale, "channel." + channel)
                    .orElse(text(channel));
        }

        private void event(ItemSystem.Events.CreateItem event) {
            if (!parent.isRoot())
                return;

            Locale locale = event.locale();
            tfbr.lc().lines(locale, lck("lore.lore"),
                    "frequency", frequency()
                            .map(f -> tfbr.lc().safe(locale, lck("lore.frequency.set"),
                                    "frequency", Frequency.of(f).name(tfbr, locale)))
                            .orElse(tfbr.lc().safe(locale, lck("lore.frequency.unset"))),
                    "channel", channel()
                            .map(c -> tfbr.lc().safe(locale, lck("lore.channel.set"),
                                    "channel", channelName(locale, c)))
                            .orElse(tfbr.lc().safe(locale, lck("lore.channel.unset"))))
                    .ifPresent(event.item()::addLore);
        }

        private void input(ItemTreeEvent.InputEvent event, PlayerUser player, String inputType, Component defaultInput,
                           BiConsumer<InventoryClickEvent, String> callback) {
            event.cancel();
            Locale locale = event.user().locale();
            AnvilGui gui = SokolPlugin.instance().guis().textInput(
                tfbr.lc().safe(locale, lck("input." + inputType + ".title")),
                defaultInput,
                meta -> meta.displayName(Components.BLANK.append(tfbr.lc().safe(locale, lck("input." + inputType + ".confirm")))),
                callback);
            int heldSlot = player.handle().getInventory().getHeldItemSlot();
            gui.setOnGlobalClick(evt -> {
                if (Guis.isInvalid(evt, false, heldSlot))
                    evt.setCancelled(true);
            });
            gui.show(player.handle());
        }

        private void event(ItemTreeEvent.InputEvent event) {
            if (!parent.isRoot())
                return;
            if (!(event.user() instanceof PlayerUser player))
                return;

            Locale locale = player.locale();
            inputs.run(event, () -> { // set_frequency
                input(event, player, "frequency",
                        frequency().map(f -> Frequency.of(f).name(tfbr, locale)).orElse(empty()),
                        (evt, inp) -> {
                    Frequency sFreq;
                    try {
                        sFreq = Frequency.of(inp);
                    } catch (NumberFormatException e) {
                        tfbr.lc().get(locale, lck("input.frequency.error_parse"),
                                "message", e.getMessage())
                                .ifPresent(c -> chatPosition.send(evt.getWhoClicked(), c));
                        return;
                    } catch (IllegalArgumentException e) {
                        tfbr.lc().get(locale, lck("input.frequency.error_arg"),
                                "message", e.getMessage())
                                .ifPresent(c -> chatPosition.send(evt.getWhoClicked(), c));
                        return;
                    }

                    double frequency = sFreq.raw();
                    Optional<Double> minFreq = parent.stats().descVal("min_frequency");
                    Optional<Double> maxFreq = parent.stats().descVal("max_frequency");
                    Component infinite = tfbr.lc().safe(locale, lck("input.frequency.infinite"));
                    if (
                            minFreq.map(v -> frequency < v).orElse(false)
                                    || maxFreq.map(v -> frequency > v).orElse(false)
                    ) {
                        tfbr.lc().get(locale, lck("input.frequency.out_of_range"),
                                "frequency", sFreq.name(tfbr, locale),
                                "min", minFreq.map(v -> Frequency.of(v).name(tfbr, locale)).orElse(infinite),
                                "max", maxFreq.map(v -> Frequency.of(v).name(tfbr, locale)).orElse(infinite),
                                "value", sFreq.name(tfbr, locale))
                                .ifPresent(c -> chatPosition.send(evt.getWhoClicked(), c));
                        return;
                    }

                    this.frequency = frequency;
                    event.slot().set(parent.root(), locale);
                    tfbr.lc().get(locale, lck("input.frequency.set"),
                            "frequency", sFreq.name(tfbr, locale))
                            .ifPresent(c -> chatPosition.send(evt.getWhoClicked(), c));
                });
            }, () -> { // set_channel
                input(event, player, "channel",
                        channel().map(Component::text).orElse(empty()),
                        (evt, channel) -> {
                    if (player.handle().hasPermission(new Permission("tfbr.system." + id() + ".channel." + channel, PermissionDefault.TRUE))) {
                        this.channel = channel;
                        event.slot().set(parent.root(), locale);
                        tfbr.lc().get(locale, lck("input.channel.set"),
                                "channel", channelName(locale, channel))
                                .ifPresent(c -> chatPosition.send(evt.getWhoClicked(), c));
                    } else {
                        tfbr.lc().get(locale, lck("input.channel.error_permission"),
                                "channel", channel) // use the raw channel string, as it might expose the "validity" of a channel otherwise
                                .ifPresent(c -> chatPosition.send(evt.getWhoClicked(), c));
                    }
                });
            });
        }

        @Override
        public void save(java.lang.reflect.Type type, ConfigurationNode node) throws SerializationException {
            node.node(KEY_FREQUENCY).set(frequency);
            node.node(KEY_CHANNEL).set(channel);
        }

        @Override
        public PersistentDataContainer save(PersistentDataAdapterContext ctx) throws IllegalArgumentException {
            PersistentDataContainer data = ctx.newPersistentDataContainer();
            if (frequency != null)
                data.set(tfbr.key(KEY_FREQUENCY), PersistentDataType.DOUBLE, frequency);
            if (channel != null)
                data.set(tfbr.key(KEY_CHANNEL), PersistentDataType.STRING, channel);
            return data;
        }
    }

    public static final class Builder {
        private final SokolPlugin platform;
        private final TFBRPlugin tfbr;
        private InputMapper inputs;
        private ChatPosition chatPosition = ChatPosition.ACTION_BAR;

        private Builder(SokolPlugin platform, TFBRPlugin tfbr) {
            this.platform = platform;
            this.tfbr = tfbr;
        }

        public SokolPlugin platform() { return platform; }
        public TFBRPlugin tfbr() { return tfbr; }

        public InputMapper inputs() { return inputs; }
        public Builder inputs(InputMapper inputs) { this.inputs = inputs; return this; }

        public ChatPosition chatPosition() { return chatPosition; }
        public Builder chatPosition(ChatPosition chatPosition) { this.chatPosition = chatPosition; return this; }

        public RadioSystem build() {
            if (inputs == null)
                throw new IllegalStateException("Argument not specified: inputs");
            return new RadioSystem(platform, tfbr, inputs, chatPosition);
        }
    }

    public static Builder builder(SokolPlugin platform, TFBRPlugin tfbr) {
        return new Builder(platform, tfbr);
    }

    private final SokolPlugin platform;
    private final TFBRPlugin tfbr;
    private final InputMapper inputs;
    private final ChatPosition chatPosition;

    private RadioSystem(SokolPlugin platform, TFBRPlugin tfbr, InputMapper inputs, ChatPosition chatPosition) {
        this.platform = platform;
        this.tfbr = tfbr;
        this.inputs = inputs;
        this.chatPosition = chatPosition;
    }

    @Override public String id() { return ID; }

    public SokolPlugin platform() { return platform; }
    public TFBRPlugin tfbr() { return tfbr; }
    public InputMapper inputs() { return inputs; }
    public ChatPosition chatPosition() { return chatPosition; }

    @Override public Map<String, Stat<?>> statTypes() { return STATS; }

    @Override
    public Instance create(TreeNode node) {
        return new Instance(node);
    }

    @Override
    public Instance load(PaperTreeNode node, java.lang.reflect.Type type, ConfigurationNode config) throws SerializationException {
        Instance result = new Instance(node);
        result.frequency = config.node(KEY_FREQUENCY).get(Double.class, (Double) null);
        result.channel = config.node(KEY_CHANNEL).get(String.class, (String) null);
        return result;
    }

    @Override
    public Instance load(PaperTreeNode node, PersistentDataContainer data) throws IllegalArgumentException {
        Instance result = new Instance(node);
        //noinspection ConstantConditions
        result.frequency = data.getOrDefault(tfbr.key(KEY_FREQUENCY), PersistentDataType.DOUBLE, null);
        //noinspection ConstantConditions
        result.channel = data.getOrDefault(tfbr.key(KEY_CHANNEL), PersistentDataType.STRING, null);
        return result;
    }

    public static PaperSystem.Type type(SokolPlugin platform, TFBRPlugin tfbr) {
        return config -> builder(platform, tfbr)
                .inputs(InputMapper.loader()
                        .actionKeys("set_frequency", "set_channel")
                        .loadFrom(config.node("inputs")))
                .chatPosition(config.node("chat_position").get(ChatPosition.class, ChatPosition.ACTION_BAR))
                .build();
    }
}
