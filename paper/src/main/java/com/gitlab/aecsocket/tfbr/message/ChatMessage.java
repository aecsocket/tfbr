package com.gitlab.aecsocket.tfbr.message;

import com.gitlab.aecsocket.minecommons.core.ChatPosition;
import org.spongepowered.configurate.objectmapping.meta.Required;

public interface ChatMessage<O extends ChatMessage.Options> extends Message<O> {
    abstract class Options extends Message.Options {
        protected @Required String chatKey;
        protected ChatPosition position;

        public Options(Obfuscation obfuscation, String chatKey, ChatPosition position) {
            super(obfuscation);
            this.chatKey = chatKey;
            this.position = position;
        }

        public Options(Options o) {
            super(o);
            chatKey = o.chatKey;
            position = o.position;
        }

        private Options() { this(Obfuscation.NONE, "", ChatPosition.CHAT); }

        public String chatKey() { return chatKey; }
        public void chatKey(String chatKey) { this.chatKey = chatKey; }

        public ChatPosition position() { return position; }
        public void position(ChatPosition position) { this.position = position; }
    }

    String content();
}
