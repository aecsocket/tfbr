package com.gitlab.aecsocket.tfbr.endpoint;

import com.gitlab.aecsocket.tfbr.TFBRPlugin;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;

/* package */ class LivingEntityEndpointImpl extends EntityEndpointImpl implements LivingEntityEndpoint {
    private final LivingEntity entity;

    public LivingEntityEndpointImpl(TFBRPlugin plugin, LivingEntity entity) {
        super(plugin, entity);
        this.entity = entity;
    }

    @Override public LivingEntity entity() { return entity; }

    @Override public Location location() { return entity.getEyeLocation(); }
}
