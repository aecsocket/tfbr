package com.gitlab.aecsocket.tfbr.message;

import com.gitlab.aecsocket.tfbr.endpoint.Endpoint;
import net.kyori.adventure.text.Component;

import java.util.function.Function;

public enum Obfuscation {
    NONE    (Endpoint::name),
    CODE    (e -> Component.text("%x".formatted(e.code())));

    private final Function<Endpoint, Component> mapper;

    Obfuscation(Function<Endpoint, Component> mapper) {
        this.mapper = mapper;
    }

    public Component map(Endpoint endpoint) {
        return mapper.apply(endpoint);
    }
}
