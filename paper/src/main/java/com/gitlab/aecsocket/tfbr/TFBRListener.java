package com.gitlab.aecsocket.tfbr;

import com.gitlab.aecsocket.sokol.paper.SokolPlugin;
import com.gitlab.aecsocket.tfbr.endpoint.EndpointCollectEvent;
import com.gitlab.aecsocket.tfbr.endpoint.PlayerEndpoint;
import com.gitlab.aecsocket.tfbr.strategy.ChatStrategy;
import io.papermc.paper.event.player.AsyncChatEvent;
import net.kyori.adventure.text.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.inventory.EquipmentSlot;

import java.util.Collection;

import static com.gitlab.aecsocket.tfbr.endpoint.Endpoint.*;

/* package */ class TFBRListener implements Listener {
    private final TFBRPlugin plugin;

    public TFBRListener(TFBRPlugin plugin) {
        this.plugin = plugin;
    }

    public TFBRPlugin plugin() { return plugin; }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void event(AsyncChatEvent event) {
        String content = ((TextComponent) event.originalMessage()).content();
        if (content.length() == 0)
            return;
        Collection<ChatStrategy> strategies = plugin.strategies().get(content.charAt(0));
        if (strategies == null)
            strategies = plugin.strategies().get(null);
        else
            content = content.substring(1);

        if (strategies != null && content.length() > 0) {
            for (ChatStrategy strategy : strategies) {
                if (strategy.applies(event)) {
                    strategy.handle(plugin, event, content);
                }
            }
        }
    }

    @EventHandler
    private void event(EndpointCollectEvent event) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            PlayerEndpoint endpoint = player(plugin, player);
            event.endpoints().add(endpoint);
            for (EquipmentSlot slot : EquipmentSlot.values()) {
                //noinspection ConstantConditions
                SokolPlugin.instance().persistenceManager().load(player.getEquipment().getItem(slot)).ifPresent(node ->
                        event.endpoints().add(equipment(endpoint, slot, node)));
            }
        }
    }
}
