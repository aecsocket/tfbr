package com.gitlab.aecsocket.tfbr.endpoint;

import com.gitlab.aecsocket.sokol.paper.PaperTreeNode;
import com.gitlab.aecsocket.tfbr.TFBRPlugin;
import com.gitlab.aecsocket.tfbr.message.Message;
import com.gitlab.aecsocket.tfbr.message.MessageSendEvent;
import com.gitlab.aecsocket.tfbr.util.ReceivesChannel;
import com.gitlab.aecsocket.tfbr.util.ReceivesFrequency;
import net.kyori.adventure.text.Component;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.EquipmentSlot;

import java.util.Locale;

public interface Endpoint extends ReceivesFrequency, ReceivesChannel {
    @FunctionalInterface
    interface Transmission {
        Message<?> message(Endpoint tx, Endpoint rx);
    }
    /*
    impl ideas:
     - player
     - entity
     - microphone
     - speaker
     - radio relay
     */

    Component name();
    int code();

    Locale locale();
    Location location();

    void receive(Message<?> message);

    default void transmit(TFBRPlugin plugin, Transmission transmission) {
        for (Endpoint rx : plugin.collectEndpoints()) {
            Message<?> message = transmission.message(this, rx);
            if (new MessageSendEvent(message).callEvent()) {
                rx.receive(message);
            }
        }
    }

    static EntityEndpoint anyEntity(TFBRPlugin plugin, Entity entity) {
        if (entity instanceof Player player)
            return player(plugin, player);
        if (entity instanceof LivingEntity living)
            return living(plugin, living);
        return entity(plugin, entity);
    }

    static EntityEndpoint entity(TFBRPlugin plugin, Entity entity) {
        return new EntityEndpointImpl(plugin, entity);
    }

    static LivingEntityEndpoint living(TFBRPlugin plugin, LivingEntity entity) {
        return new LivingEntityEndpointImpl(plugin, entity);
    }

    static PlayerEndpoint player(TFBRPlugin plugin, Player entity) {
        return new PlayerEndpointImpl(plugin, entity);
    }

    static EquipmentEndpoint equipment(LivingEntityEndpoint entity, EquipmentSlot slot, PaperTreeNode node) {
        return new EquipmentEndpointImpl(entity, slot, node);
    }
}
