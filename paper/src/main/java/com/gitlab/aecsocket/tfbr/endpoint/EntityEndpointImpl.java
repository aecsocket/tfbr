package com.gitlab.aecsocket.tfbr.endpoint;

import com.gitlab.aecsocket.tfbr.TFBRPlugin;
import com.gitlab.aecsocket.tfbr.message.Message;
import net.kyori.adventure.key.Key;
import net.kyori.adventure.text.Component;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.persistence.PersistentDataType;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Locale;
import java.util.Objects;
import java.util.Optional;

/* package */ class EntityEndpointImpl implements EntityEndpoint {
    public static final String KEY_RADIO_FREQUENCY = "radio_frequency";
    public static final String KEY_RADIO_CHANNEL = "radio_channel";

    private final TFBRPlugin plugin;
    private final Entity entity;

    public EntityEndpointImpl(TFBRPlugin plugin, Entity entity) {
        this.plugin = plugin;
        this.entity = entity;
    }

    public TFBRPlugin plugin() { return plugin; }
    @Override public Entity entity() { return entity; }

    @Override
    public Component name() {
        Key key = entity().getType().getKey();
        return entity().customName() == null
                ? Component.translatable("entity." + key.namespace() + "." + key.value())
                : entity().customName();
    }

    @Override public int code() { return entity().getUniqueId().hashCode(); }
    @Override public Locale locale() { return plugin.defaultLocale(); }
    @Override public Location location() { return entity.getLocation(); }

    @Override public void receive(Message<?> message) {}

    protected <T> T get(String key, PersistentDataType<?, T> type) {
        return entity.getPersistentDataContainer().get(plugin.key(key), type);
    }

    protected <T> T get(String key, PersistentDataType<?, T> type, T def) {
        return entity.getPersistentDataContainer().getOrDefault(plugin.key(key), type, def);
    }

    protected <T> void set(String key, PersistentDataType<?, T> type, @Nullable T value) {
        if (value == null)
            entity.getPersistentDataContainer().remove(plugin.key(key));
        else
            entity.getPersistentDataContainer().set(plugin.key(key), type, value);
    }

    @Override
    public Optional<Double> frequency() {
        return Optional.ofNullable(get(KEY_RADIO_FREQUENCY, PersistentDataType.DOUBLE));
    }

    @Override
    public void frequency(@Nullable Double frequency) {
        set(KEY_RADIO_FREQUENCY, PersistentDataType.DOUBLE, frequency);
    }

    @Override
    public Optional<String> channel() {
        return Optional.ofNullable(get(KEY_RADIO_CHANNEL, PersistentDataType.STRING));
    }

    @Override
    public void channel(@Nullable String channel) {
        set(KEY_RADIO_CHANNEL, PersistentDataType.STRING, channel);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EntityEndpointImpl that = (EntityEndpointImpl) o;
        return entity.equals(that.entity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(plugin, entity);
    }

    @Override public String toString() { return "[" + entity + "]"; }
}
