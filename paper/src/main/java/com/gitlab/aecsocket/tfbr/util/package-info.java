/**
 * Generic utilities for radios.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.tfbr.util;
